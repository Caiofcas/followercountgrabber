"""
# My first app
Here's our first attempt at using data to create a table:
"""
import re
import os
import requests
import streamlit as st

# DEBUG = os.environ.get('DEBUG', True)
DEBUG = os.environ.get('DEBUG', False)

followers_re = re.compile("followers=\"\d+\"")


def header():
    st.title("Instagram Follower Graber", anchor="Title")
    st.markdown("""
    Coleta o número de seguidores de várias contas utilizando o site grahmhir.com.

    Pesquise a conta que você quer no site, e selecioe a opção correta, você vai ver
    que a url é da forma:

    *https://gramhir.com/profile/csgoniko/2951277128*

    Pegue a parte depois de *'profile/'* e coloque na caixa de texto abaixo para cada 
    conta que quiser rastrear.
    """)


def read_accounts():
    # TODO: read more than one
    read = st.text_area(
        label="Contas que você quer rastrear, separadas por ',' ou quebra de linha.",
        value="s1mpleo/529464688\nb1tcsgo/6857333620\ncsgoniko/2951277128"
    )

    if DEBUG:
        st.write(read)

    return read


header()

accounts = read_accounts()

if accounts is not None:
    rows = []
    for acc in accounts.splitlines():
        try:
            nome, id = acc.split('/')
        except:
            st.write(f"Entrada {acc} não está no formato correto: "
                     "<nome>/<id_gramhir>.")
            continue
        # TODO await to do requests until read is set
        r = requests.get(f"https://gramhir.com/profile/{acc}")

        if DEBUG:
            st.write(r.status_code)

        if r.status_code == 200:
            page = r.content.decode('UTF-8')
            f_count = followers_re.search(page)
            if DEBUG:
                st.write(f_count)
            f_count = f_count[0].strip("folowers=\"").strip("\"")
            if DEBUG:
                st.write(f_count)
        else:
            f_count = "Não foi possível obter #Seguidores."
            st.write(f"Não foi possível obter #Seguidores para conta: {nome}.")

        rows.append({
            "conta": nome,
            "#Seguidores": f_count,
        })

if len(rows) > 0:
    st.header("Conta: Seguidores")
    st.write('\n'.join(
        [f"{d['conta'] :<10}: {d['#Seguidores']:<15}"
         for d in rows]
    ))
